package api.entities;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.time.LocalDate;

@Entity
public class Person implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;

    @Size(max = 20)
    @Column(unique = true)
    private String dni;

    private int age;
    private LocalDate birthday;
    private boolean married; // true or false
    private double height; // in 1.64

    private String photo; // url


    // Builders
    public Person() {
    }

    public Person(String name, String dni, int age, LocalDate birthday, boolean married, double height) {
        this.name = name;
        this.dni = dni;
        this.age = age;
        this.birthday = birthday;
        this.married = married;
        this.height = height;
    }

    public Person(String name, @Size(max = 20) String dni, int age, LocalDate birthday, boolean married, double height, String photo) {
        this.name = name;
        this.dni = dni;
        this.age = age;
        this.birthday = birthday;
        this.married = married;
        this.height = height;
        this.photo = photo;
    }

// Setter and Getter

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDni() {
        return dni;
    }

    public void setDni(String dni) {
        this.dni = dni;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public LocalDate getBirthday() {
        return birthday;
    }

    public void setBirthday(LocalDate birthday) {
        this.birthday = birthday;
    }

    public boolean isMarried() {
        return married;
    }

    public void setMarried(boolean married) {
        this.married = married;
    }

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }
}
