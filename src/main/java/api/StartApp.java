package api;

import api.controller.PersonController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.io.File;

@SpringBootApplication
public class StartApp {
    public static void main(String[] args) {
        SpringApplication.run(StartApp.class, args); // Start application

        final Logger log = LoggerFactory.getLogger(StartApp.class);
        File image;
        // Creating directory for images
        image = new File("image");
        if (image.exists()) {
            log.info("Directory exists");
        }else {
            image.mkdir();
            log.info("Directory created!");
        }
    }
}
