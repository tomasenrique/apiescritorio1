POST http://localhost:8081/api2/person/add
Content-Type: application/json

{
  "name": "Tomas Estrada",
  "dni": "43588075q",
  "age": 40,
  "birthday": "1980-01-07",
  "married": true,
  "height": 1.64
}

###

POST http://localhost:8081/api2/person/add/multipart

###

GET http://localhost:8081/api2/person/show

###

GET http://localhost:8081/api2/person/show/paginationDefecto

###

GET http://localhost:8081/api2/person/show/paginationUsuario?page=0&size=5

###



