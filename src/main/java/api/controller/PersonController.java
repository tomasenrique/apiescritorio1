package api.controller;

import api.entities.Person;
import api.repository.PersonRepository;
import api.repository.PersonRepositoryPagination;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;


@RestController
@RequestMapping(path = "/api2/person")
public class PersonController {

    private static final Logger log = LoggerFactory.getLogger(PersonController.class);
    private static final String DIRECTORY = "image";


    @Autowired
    private PersonRepository personRepository;

    @Autowired
    private PersonRepositoryPagination personRepositoryPagination;


    @PostMapping("/add")
    public ResponseEntity<Person> addPerson(@RequestBody Person person) {
        try {
            personRepository.save(person);
            return ResponseEntity.status(HttpStatus.CREATED).body(person);
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Can't add person.");
        }
    }

    // =================================================================================================================


    @PostMapping("/add/multipart")
    @ResponseBody
//    @RequestMapping(value = "/add/multipart", method = RequestMethod.POST , consumes = "multipart/form-data")
//    public ResponseEntity<Person> addPersonImagen(@RequestBody Person person, @RequestParam("file") MultipartFile file) {
    public ResponseEntity<?> addPersonImagen(@RequestParam MultipartFile file) {
//    public void addPersonImagen() {
        Path route = Paths.get(DIRECTORY);

        if (file == null || file.isEmpty()) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "There isn't imagen.");
        } else {
            if (Files.isDirectory(route)) {
//            log.info("Directory exists");
//            log.info("Route: " + route.getFileName() + " => " + route.toAbsolutePath());

                StringBuilder builder = new StringBuilder();
                builder.append(route.toAbsolutePath());
                builder.append(File.separator);
                builder.append(file.getOriginalFilename()); // esto sera el nombre del archivo

                try {
                    byte[] fileBytes = file.getBytes(); // aqui se obtendra los bytes del archivo recibido
                    Path path = Paths.get(builder.toString()); // esto sera la ruta del archivo
                    Files.write(path, fileBytes); // esto indicara en que archivo se tendra que escribir la app los bytes
                } catch (IOException e) {
                    e.printStackTrace();
                }
//                person.setPhoto(builder.toString()); // agrega la ruta de la imagen
//                personRepository.save(person);
//                return ResponseEntity.status(HttpStatus.CREATED).body(person);
                throw new ResponseStatusException(HttpStatus.OK, "imagen agregada.");

            } else {
                log.info("Directory not exists");
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "There isn't imagen directory.");
            }
        }
    }

    // =================================================================================================================

    @GetMapping("/show")
    public Iterable<Person> getPersons() {
        try {
            return personRepository.findAll();
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.NO_CONTENT, "There is not persons in database.");
        }
    }


    /**
     * this method it serves to get pagination from system
     * to see in file 'application.properties'
     * spring.data.rest.page-param-name=page
     * spring.data.rest.limit-param-name=limit
     *
     * @param pageable
     * @return one list of person
     *
     * http://localhost:8081/api2/person/show/paginationSystem  ==>> Default 20 elements for page
     * http://localhost:8081/api2/person/show/paginationSystem?size=10  ==>> Return the number of elements give them user
     * http://localhost:8081/api2/person/show/paginationSystem?page=10&size=5  ==>> Return the number of page and elements quantity for page
     */
    @GetMapping("/show/paginationSystem")
    public List<Person> getPersonsPaginationSystem(Pageable pageable) {

        // with 'getContent()' to get the number of page and tha quantity of elements for page from system
        List<Person> resultados = personRepositoryPagination.findAll(pageable).getContent();

        if (resultados.isEmpty()) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "no funciona");
        } else {
            return resultados;
        }
    }

    // this method it serves to default pagination
    // http://localhost:8081/api2/person/show/paginationDefect
    @GetMapping("/show/paginationDefect")
    public Page<Person> getPersonsPaginationDefect(@PageableDefault(size = 10, page = 0) Pageable pageable) {
        Page<Person> resultados = personRepositoryPagination.findAll(pageable);

        if (resultados.isEmpty()) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "no funciona");
        } else {
            return resultados;
        }
    }


    /**
     * This method it serves for users to give pagination personalized
     *
     * @param page will be the number of page
     * @param size will be the number of elements for page
     * @return one object of Page type with contening one list of person type.
     * <p>
     * http://localhost:8081/api2/person/show/paginationUser?page=0&size=5
     */
    @GetMapping("/show/paginationUser")
    public Page<Person> getPersonsPaginationUser(@RequestParam int page, int size) {
        Pageable pageable = PageRequest.of(page, size);
        Page<Person> resultados = personRepositoryPagination.findAll(pageable);

        if (resultados.isEmpty()) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "no funciona");
        } else {
            return resultados;
        }
    }


    // =================================================================================================================
}
