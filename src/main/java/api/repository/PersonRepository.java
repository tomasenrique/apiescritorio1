package api.repository;

import api.entities.Person;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PersonRepository extends JpaRepository<Person, Long> {

    Person findPersonByDni(String dni);

    Person findPersonByName(String name);

    List<Person> findPersonByMarried(boolean married);

//    List<Person> findPersonByAgeAndAge(int minAge, int maxAge);

    List<Person> findPersonByAgeBetween(int minAge, int maxAge);



}
