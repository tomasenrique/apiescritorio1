package api.repository;

import api.entities.Person;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PersonRepositoryPagination extends JpaRepository<Person, PagingAndSortingRepository<Person, Long>> {


    Page<Person> findAll(Pageable pageable);

}
